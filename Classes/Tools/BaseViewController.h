//
//  BaseViewController.h
//  MainProject
//
//  Created by 徐明 on 2020/5/16.
//  Copyright © 2020 XM. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
